package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

//func TestHandler1(t *testing.T) {
//	request := events.APIGatewayProxyRequest{}
//	response, err := handler(request)
//	assert.Nil(t, err)
//	assert.Equal(t, 200, response.StatusCode)
//	assert.Equal(t, "text/css; charset=utf-8", response.Headers["Content-Type"])
//	assert.Equal(t, "no-cache, no-store, must-revalidate", response.Headers["Cache-Control"])
//}

func TestCleanIPAddr(t *testing.T){
	assert.Equal(t, "8.8.8.XX", cleanForwardedFor("8.8.8.8"))
	assert.Equal(t, "8.8.8.XX, 8.8.4.XX", cleanForwardedFor("8.8.8.8, 8.8.4.4"))
}