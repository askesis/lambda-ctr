package main

import (
	"crypto/rand"
	"encoding/base64"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"log"
	"strings"
	"time"
)

func randomEtag() (string, error) {
	var data = make([]byte, 32)

	var _, e = rand.Read(data)

	if e != nil {
		return "", e
	}

	return base64.RawStdEncoding.EncodeToString(data), nil
}

func unsafeEtag() string {
	var etag, _ = randomEtag()
	return etag
}

func establishDynamoDb() (*dynamodb.DynamoDB, error){

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String("eu-central-1"),
	})

	if err != nil{
		return nil, err;
	}

	return dynamodb.New(sess), nil;
}

func requestToAttrMap(request events.APIGatewayProxyRequest) (map[string]*dynamodb.AttributeValue, error){
	preConversion := make(map[string]interface{});

	preConversion["RandomPartition"] = unsafeEtag();

	for header := range request.Headers{
		preConversion["Header:" + header] = request.Headers[header];
	}

	preConversion["RequestUnixTime"] = time.Now().Unix();
	preConversion["RequestRFCTime"] = time.Now().UTC().Format(time.RFC3339)

	forwardedHeader, contains := request.Headers["X-Forwarded-For"]

	if contains{
		preConversion["Header:X-Forwarded-For"] = cleanForwardedFor(forwardedHeader);
	}

	return dynamodbattribute.MarshalMap(preConversion);
}

func recordInDB(request events.APIGatewayProxyRequest){
	db, err := establishDynamoDb();

	if err != nil{
		log.Printf("Error %s when conecting to dynamo", err)
		return;
	}

	marshaled, err := requestToAttrMap(request);

	if err != nil{
		log.Printf("Error %s converting the attrs", err)
		return;
	}

	_, err = db.PutItem(&dynamodb.PutItemInput{
		TableName: aws.String("BlogVisits"),
		Item:marshaled,
	})

	if err != nil{
		log.Printf("Error %s on Put", err)
		return;
	}
}

func cleanForwardedFor(header string) string{
	ipAddrs := strings.Split(header, ",")
	for  ii:=0; ii<len(ipAddrs); ii++{
		addr := strings.Trim(ipAddrs[ii], " ");
		addrParts := strings.Split(addr, ".")
		if len(addrParts) > 0{
			addrParts[len(addrParts) - 1] = "XX"
		}
		addr = strings.Join(addrParts, ".")
		ipAddrs[ii] = addr;
	}
	return strings.Join(ipAddrs, ", ");
}

func getContentType(request events.APIGatewayProxyRequest) string{
	if val, ok := request.Headers["Content-Type"]; ok {
		return val;
	}

	return "text/css; charset=utf-8";
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	defer recordInDB(request);

	return events.APIGatewayProxyResponse{
		Body:       "// Empty css file returned to monitor pageloads",
		StatusCode: 200,
		Headers: map[string]string{
			"Content-Type":  getContentType(request),
			"X-Purpose":     "This is used for tracking visits to the page. See privacy policy for details",
			"Etag":          unsafeEtag(),
			"Cache-Control": "no-cache, no-store, must-revalidate",
		},
	}, nil

}

func main() {
	lambda.Start(handler)
}
