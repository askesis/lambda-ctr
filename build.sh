#!/usr/bin/env bash

ROOT=$(pwd)
DEP=${DEP_LOCATION:-dep-linux-0.5.0}
GOOS=linux

rm -rf .build

mkdir -p .build


cd src/lambdactr

${DEP} ensure

golint -set_exit_status

go vet

go test

go build -o ${ROOT}/.build/lambdactr

cd ${ROOT}/.build

chmod +x lambdactr

zip handler.zip ./lambdactr


cd ${ROOT}

